def walk_code(line, start):
	pos = start

	for ch in line:
		if ch == "U":
			pos = (pos[0], max(pos[1] - 1, 0))
		elif ch == "R":
			pos = (min(pos[0] + 1, 2), pos[1])
		elif ch == "D":
			pos = (pos[0], min(pos[1] + 1, 2))
		elif ch == "L":
			pos = (max(pos[0] - 1, 0), pos[1])

	digits = [1, 2, 3, 4, 5, 6, 7, 8, 9]
	return digits[3 * pos[1] + pos[0]], pos

def walk_code_2(line, start):
	pos = start

	digits = [
		0, 0, 1, 0, 0,
		0, 2, 3, 4, 0,
		5, 6, 7, 8, 9,
		0, 'A', 'B', 'C', 0,
		0, 0, 'D', 0, 0
	]

	for ch in line:
		if ch == "U":
			next_pos = (pos[0], max(pos[1] - 1, 0))
		elif ch == "R":
			next_pos = (min(pos[0] + 1, 4), pos[1])
		elif ch == "D":
			next_pos = (pos[0], min(pos[1] + 1, 4))
		elif ch == "L":
			next_pos = (max(pos[0] - 1, 0), pos[1])

		if digits[5 * next_pos[1] + next_pos[0]] != 0:
			pos = next_pos

	return digits[5 * pos[1] + pos[0]], pos

if __name__ == "__main__":
	first_star = []
	first_star_start = (1, 1)
	second_star = []
	second_star_start = (0, 2)

	with open('input.txt') as f:
		for line in f:
			digit, first_star_start = walk_code(line, first_star_start)
			first_star.append(digit)
			digit, second_star_start = walk_code_2(line, second_star_start)
			second_star.append(digit)

	print("first star: {}".format(first_star))
	print("second star: {}".format(second_star))