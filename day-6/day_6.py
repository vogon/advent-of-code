LINE_LENGTH = 8

if __name__ == '__main__':
	freqs = [dict.fromkeys("abcdefghijklmnopqrstuvwxyz", 0) 
		for i in range(LINE_LENGTH)]

	with open('input.txt') as f:
		for line in f:
			for i in range(LINE_LENGTH):
				freqs[i][line[i]] += 1

	code = ""
	code_2 = ""

	for i in range(LINE_LENGTH):
		sorted_freqs = sorted(freqs[i], key = lambda k: freqs[i][k], reverse = True)
		code += sorted_freqs[0]
		code_2 += sorted_freqs[-1]

	print("first star: {}".format(code))
	print("second star: {}".format(code_2))