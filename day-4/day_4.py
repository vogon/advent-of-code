import re

class Room:
	def __init__(self, s):
		regex = r"([a-z]+(?:-[a-z]+)*)-([0-9]+)\[([a-z]{5})\]"
		m = re.search(regex, s)

		self.name = m.group(1)
		self.sector_id = int(m.group(2))
		self.checksum = m.group(3)

	def is_decoy(self):
		freqs = dict.fromkeys("abcdefghijklmnopqrstuvwxyz", 0)

		for ch in self.name:
			if ch == '-':
				continue

			freqs[ch] += 1

		checksum_letters = "abcdefghijklmnopqrstuvwxyz"
		checksum_letters = sorted(checksum_letters, key = lambda k: freqs[k], reverse = True)
		# print("checksum letters for {}-{}[{}] are {}".format(self.name, self.sector_id, self.checksum, checksum_letters[0:5]))

		for cksum_i in range(len(self.checksum)):
			if checksum_letters[cksum_i] != self.checksum[cksum_i]:
				return True

		return False

	def decrypted_name(self):
		alphabet = "abcdefghijklmnopqrstuvwxyz"
		decrypted = ""

		for ch in self.name:
			if ch == '-':
				decrypted += ' '
			else:
				i = (alphabet.index(ch) + self.sector_id) % len(alphabet)
				decrypted += alphabet[i]

		return decrypted

if __name__ == "__main__":
	first_star_id_sum = 0

	with open('input.txt') as f:
		for line in f:
			rm = Room(line)

			if not rm.is_decoy():
				first_star_id_sum += rm.sector_id
				print("{}-{}[{}]: {}".format(rm.name, rm.sector_id, rm.checksum, rm.decrypted_name()))

	print("first star: {}".format(first_star_id_sum))