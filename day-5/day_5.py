import hashlib

if __name__ == "__main__":
	i = 0
	password = ""
	password_2 = [None] * 8

	while True:
		md5 = hashlib.md5()

		md5.update("uqwqemis{}".format(i).encode('utf-8'))
		h = md5.hexdigest()

		if h[0:5] == "00000":
			if len(password) < 8:
				password += h[5]

			if h[5] in '01234567':
				index = int(h[5])

				if password_2[index] == None:
					password_2[index] = h[6]

				if None not in password_2:
					break

			print("interesting hash found: {} ({}, {}: {})".format(i, h[5], h[6], password_2))

		i += 1

	print("first star: {}".format(password))
	print("second star: {}".format(password_2))