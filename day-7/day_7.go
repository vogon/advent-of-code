package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
)

func contains_abba(field string) bool {
	for i := 0; i < len(field) - 3; i++ {
		if field[i] == field[i + 3] && field[i + 1] == field[i + 2] && 
				field[i] != field[i + 1] /* to prevent AAAA sequences */ {
			return true
		}
	}

	return false
}

func contained_abas(supernet string) []string {
	result := []string{}

	for i := 0; i < len(supernet) - 2; i++ {
		if supernet[i] == supernet[i + 2] {
			result = append(result, supernet[i:i+3])
		}
	}

	return result
}

func search_babs(hypernet string, abas []string) bool {
	for _, aba := range abas {
		// invert the aba into its corresponding bab
		bab := fmt.Sprintf("%c%c%c", aba[1], aba[0], aba[1])

		if strings.Index(hypernet, bab) != -1 {
			return true
		}
	}

	return false
}

func supports_tls(ip string) bool {
	// find hypernet strings
	re, _ := regexp.Compile("\\[[a-z]+\\]")
	m := re.FindAllStringIndex(ip, -1)

	result := false
	last_hypernet_end := 0

	for _, hypernet_indices := range m {
		supernet := ip[last_hypernet_end:hypernet_indices[0]]

		if contains_abba(supernet) {
			result = true
		}

		hypernet := ip[hypernet_indices[0] + 1:hypernet_indices[1] - 1]

		if contains_abba(hypernet) {
			return false
		}

		last_hypernet_end = hypernet_indices[1]
	}

	last_supernet := ip[last_hypernet_end:]

	if contains_abba(last_supernet) {
		result = true
	}

	return result
}

func supports_ssl(ip string) bool {
	// find hypernet strings
	re, _ := regexp.Compile("\\[[a-z]+\\]")
	m := re.FindAllStringIndex(ip, -1)

	last_hypernet_end := 0
	all_abas := []string{}

	for _, hypernet_indices := range m {
		supernet := ip[last_hypernet_end:hypernet_indices[0]]

		all_abas = append(all_abas, contained_abas(supernet)...)

		last_hypernet_end = hypernet_indices[1]
	}

	last_supernet := ip[last_hypernet_end:]
	all_abas = append(all_abas, contained_abas(last_supernet)...)

	for _, hypernet_indices := range m {
		hypernet := ip[hypernet_indices[0] + 1:hypernet_indices[1] - 1]

		if search_babs(hypernet, all_abas) {
			return true
		}
	}

	return false
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	first_star_count := 0
	second_star_count := 0

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		ip := scanner.Text()

		if supports_tls(ip) {
			first_star_count += 1
		}

		if supports_ssl(ip) {
			second_star_count += 1
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	fmt.Printf("first star: %d\n", first_star_count)
	fmt.Printf("second star: %d\n", second_star_count)
}