def is_possible(tri):
	if (tri[0] + tri[1]) <= tri[2]:
		return False
	elif (tri[0] + tri[2]) <= tri[1]:
		return False
	elif (tri[1] + tri[2]) <= tri[0]:
		return False
	else:
		return True

if __name__ == "__main__":
	first_star_count = 0
	second_star_count = 0

	with open('input.txt') as f:
		tri_buf = ([], [], [])

		for line in f:
			sides = [int(side) for side in line.split()]

			first_star_count += (1 if is_possible(sides) else 0)

			tri_buf[0].append(sides[0])
			tri_buf[1].append(sides[1])
			tri_buf[2].append(sides[2])

			if len(tri_buf[0]) == 3:
				second_star_count += (1 if is_possible(tri_buf[0]) else 0)
				second_star_count += (1 if is_possible(tri_buf[1]) else 0)
				second_star_count += (1 if is_possible(tri_buf[2]) else 0)

				tri_buf[0].clear()
				tri_buf[1].clear()
				tri_buf[2].clear()

	print("first star: {}".format(first_star_count))
	print("second star: {}".format(second_star_count))