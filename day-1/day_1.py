from enum import Enum

class Direction(Enum):
	north = (0, 1)
	east = (1, 0)
	south = (0, -1)
	west = (-1, 0)

	def turn_right(self):
		if self == Direction.north:
			return Direction.east
		elif self == Direction.east:
			return Direction.south
		elif self == Direction.south:
			return Direction.west
		elif self == Direction.west:
			return Direction.north

	def turn_left(self):
		if self == Direction.north:
			return Direction.west
		elif self == Direction.west:
			return Direction.south
		elif self == Direction.south:
			return Direction.east
		elif self == Direction.east:
			return Direction.north

	def __mul__(self, other):
		return (self.value[0] * other, self.value[1] * other)

def walk_path(str, second_star):
	direction = Direction.north
	r = (0, 0)
	r_history = set([r])
	path_components = str.strip().split(",")

	for component in path_components:
		component = component.strip()

		if component[0] == "L":
			direction = direction.turn_left()
		elif component[0] == "R":
			direction = direction.turn_right()

		leg_distance = int(component[1:])

		if second_star:
			for i in range(leg_distance):
				r = (r[0] + direction.value[0], r[1] + direction.value[1])

				if r in r_history:
					return r
				else:
					r_history.add(r)
		else:
			leg = direction * leg_distance
			r = (r[0] + leg[0], r[1] + leg[1])

	return r

if __name__ == "__main__":
	directions = '''
		L1, L3, L5, L3, R1, L4, L5, R1, R3, L5, R1, L3, L2, L3, R2, R2, L3, L3, R1, L2, R1, L3, 
		L2, R4, R2, L5, R4, L5, R4, L2, R3, L2, R4, R1, L5, L4, R1, L2, R3, R1, R2, L4, R1, L2,
		R3, L2, L3, R5, L192, R4, L5, R4, L1, R4, L4, R2, L5, R45, L2, L5, R4, R5, L3, R5, R77,
		R2, R5, L5, R1, R4, L4, L4, R2, L4, L1, R191, R1, L1, L2, L2, L4, L3, R1, L3, R1, R5,
		R3, L1, L4, L2, L3, L1, L1, R5, L4, R1, L3, R1, L2, R1, R4, R5, L4, L2, R4, R5, L1, L2,
		R3, L4, R2, R2, R3, L2, L3, L5, R3, R1, L4, L3, R4, R2, R2, R2, R1, L4, R4, R1, R2, R1,
		L2, L2, R4, L1, L2, R3, L3, L5, L4, R4, L3, L1, L5, L3, L5, R5, L5, L4, L2, R1, L2, L4,
		L2, L4, L1, R4, R4, R5, R1, L4, R2, L4, L2, L4, R2, L4, L1, L2, R1, R4, R3, R2, R2, R5,
		L1, L2
	'''

	print("first star: {}".format(walk_path(directions, False)))
	print("second star: {}".format(walk_path(directions, True)))